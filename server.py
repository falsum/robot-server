from tornado.options import options, define, parse_command_line
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi
import tornado.websocket
import json
import os
import traceback
from pololu_drv8835_rpi import motors, MAX_SPEED

class Command(object):
    def __init__(self, j):
        self.__dict__ = json.loads(j)

class RobotHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        print ("\nWebSocket opened")

    def on_message(self, message):
        print ("msg recevied", message)
        command = Command(message)
        if command.command == 'direction':
            print(command.l)
            print(command.r)
            motors.setSpeeds(int(command.l * 4.8), int(command.r * 4.8))

    def on_close(self):
        print ("WebSocket closed")
        
def make_app():
    return tornado.web.Application([
        (r"/", RobotHandler), 
    ])

def main():
    motors.setSpeeds(0, 0)
    try:
        app = make_app()
        app.listen(9999)
        tornado.ioloop.IOLoop.current().start()
        # os.system("sudo uv4l --driver raspicam --auto-video_nr --width 240 --height 180 --encoding mjpeg --server-option '--port=8888'")
    except Exception:
        traceback.print_exec()
    finally:
        motors.setSpeeds(0, 0)
        # os.system("sudo pkill uv4l")

if __name__ == '__main__':
    main()